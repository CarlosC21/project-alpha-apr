## Task Manager

## Starting Project

- pip install django-markdownify
- python manage.py runserver

## The Project

- This is a project and task manager application. It is composed of projects, tasks, and the ability to login, logout, and sign up for the application
- Task Manager is composed of seven HTML templates, seven views, and two application-specifc models, the Project model and the Task model.

## Quality of Code

Cleaned code using extensions:

- flake8
- black
- djhtml

## Paths

| Path                  | Description                                             |
| --------------------- | ------------------------------------------------------- |
| /admin                | Admin page to add tasks and projects to accounts        |
| /projects/            | Page to list all the projects created                   |
| /projects/create      | Page to create a project                                |
| /projects/`<int:pk>/` | Page to see specific project created                    |
| /tasks/mine/          | Page to show accounts tasks                             |
| /tasks/create/        | Page to create a task in the account                    |
| /accounts/login       | Page to login into project and task manager application |
| /accounts/logout      | Page to show user that they have logged out             |
| /accounts/signup      | Page to signup to create own account                    |
